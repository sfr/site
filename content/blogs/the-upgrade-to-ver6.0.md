+++
title =  "The Upgrade To Version 6.0"
date =   "2023-01-24"
description = "Moving To Zola"
+++

I have rewroted my site for the 6th time and the reason is bc i wanted to move to zola (and maybe bc of rust lol)

Now the site has

- Cleaner codebase.
- More modularity. 
- Better speeds.
- Less needs for using image hosters, everything here is handled by zola.
- Changed license to [agpl-3](https://www.gnu.org/licenses/agpl-3.0.en.html).

And thats it hope you have a great day